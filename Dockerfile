###############################################################################
FROM node:16.11.1-alpine AS builder
WORKDIR /app
COPY . .
RUN yarn install --silent
RUN yarn build

###############################################################################
FROM node:16.11.1-alpine
WORKDIR /app
RUN yarn global add serve
COPY --from=builder /app/dist .
EXPOSE 8080
CMD ["serve", "-p", "8080", "."]