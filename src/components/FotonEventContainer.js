import React from 'react';
import LastestDatatable from './LastestDatatable';
import axios from 'axios'

class FotonEventContainer extends React.Component{
    constructor(props){
        super(props)

        this.state = {
            data : [],
            time: ''
        }
    }
    componentDidMount () {
        
        try{
            axios.post('https://electric.copiloto.pro/copev/public/fotonEvent',{
                license_plate: ['PGPT29','PFTW66']
            })
            .then(res => {
                const dat = res.data.data
                this.interval = setInterval(() => this.setState({data:dat, time: Date.now() }), 3000);
            })
            .catch(e =>{
                console.log(e)
            })
            
        }
        catch{

        }

    }

      componentWillUnmount() {
        clearInterval(this.interval);
      }
    
    render () {
        const data = this.state.data
        const timer = this.state.time
        return <LastestDatatable data={data} time={timer}/>
    }

}



export default FotonEventContainer;