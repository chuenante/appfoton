import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button,Menu, MenuItem,AppBar, Toolbar, IconButton, Typography} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu'
import logo from '../assets/img/branding.png'
import {Link,BrowserRouter as Router, Route} from 'react-router-dom'
import Dashboard from '../views/Dashboard';
import Acummulatedperday from '../views/Acummulatedperday';
import Summary from '../views/Summary';
import { VisibilityRounded } from '@material-ui/icons';

const useStyles = makeStyles(()=>({
    root:{
        flexGrow: 1
    },
    menuButton:{
        marginRight: '16px'    
    },
    title:{
        flexGrow: 1
    },
    colornav:{
        backgroundColor: 'rgb(17, 77, 105)'
    },
    a: {
        color:'white',
        textDecoration: 'none'
    }

}));




function Navbar(props) {
const HandleMenu = () =>{
    setOpenMenu(true)
}
const closeMenu=()=> {
    setOpenMenu(false)
}

    const classes = useStyles();
    const [openMenu, setOpenMenu] = useState();

    

    return (
        <Router className={classes.root}>
            <AppBar position="static" className={classes.colornav}> 
                <Toolbar>
                    {/* <IconButton onClick={HandleMenu} edge="start" className={classes.menuButton} color="inherit">
                        <MenuIcon  />
                    </IconButton> */}
                    {/* <Menu open={openMenu} onClose={closeMenu}>
                        <MenuItem onClick={closeMenu}></MenuItem>
                        <MenuItem onClick={closeMenu}></MenuItem>
                        <MenuItem onClick={closeMenu}><a href="/summary" >Summary</a></MenuItem>
                    </Menu> */}
                    <Typography variant="h6" className={classes.title} >
                        {props.title}
                    </Typography>
                    <Typography variant="h6" className={classes.title} >
                    <Button><a href="/" className={classes.a}>Dashboard</a></Button>
                    </Typography>
                    <Typography variant="h6" className={classes.title} >
                    <Button ><a href="/info" className={classes.a}>Info</a></Button>
                    </Typography>
                    <Typography variant="h6" className={classes.title} >
                    <Button><a href="/summary" className={classes.a}>Summary</a></Button>
                    </Typography>
                    
                    <IconButton color="inherit">
                        <img src={logo} width="200px" height="70px" className={classes.ico} />
                    </IconButton>
                </Toolbar>
            </AppBar>
        </Router>
    );
}

export default Navbar;