import React from 'react';
import { makeStyles } from '@material-ui/core/styles'
import { Card, CardContent, Typography } from '@material-ui/core';

function EnergyConsumed(props) {
    const useStyles = makeStyles(() => ({
        root: {
            textAlign: 'center',
            background: props.color
        },
        texto:{
            fontSize: 28,
            color: props.font
        },
        titulo:{
            fontSize: 22,
            fontWeight: 'bold',
            color: props.font
        }
    }));

const classes = useStyles();
return (
    <Card elevation={4} className={classes.root}>
        <CardContent>
            {props.icono}
            <Typography className={classes.titulo}>
                {props.titulo}
            </Typography>
            <Typography className={classes.texto}>
                {props.texto}
            </Typography>
        </CardContent>
    </Card>
);

}



export default EnergyConsumed;