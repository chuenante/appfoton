import React, { useState, useEffect } from 'react';
import { Line } from '@ant-design/charts';

const KmPerDay = () => {
  const [data, setData] = useState([]);
  const body = {
    license_plate: ["PGPT29", "PFTW66"],
    from: '2021-10-01 00:00:00',
    to: '2021-11-01 23:59:00'
  }

  useEffect(() => {
    asyncFetch();
  }, []);
  const asyncFetch = () => {
    fetch('https://electric.copiloto.pro/copev/public/fotonAllEvent', {
      method: 'post',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then((response) => response.json())
      .then((json) => setData(json.data)/*console.log(json)*/)
      .catch((error) => {
        console.log('Data failed FETCH', error);
      });
  };


  
  var config = {
    data: data,
    padding: 'auto',
    xField: 'event_time',
    yField: ['battery_porcentage'],
    //seriesField: [''],
    xAxis: { tickCount: 1 },
    slider: {
      start: 0.1,
      end: 1.0,
    }
  }
  return <Line {...config} />;
};


export default KmPerDay;