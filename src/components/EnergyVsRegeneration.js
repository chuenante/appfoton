import "../assets/css/Graphics.css"
import React, { useState, useEffect } from 'react';
import { Line } from '@ant-design/charts';


const body = {
  license_plate: ["PFTW66"],
  from: '2021-11-08' + ' 00:00:00',
  to: '2021-11-08' + ' 23:59:00'
}

// var localDate = new Date(utcDate);

const EnergyVsRegeneration = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    asyncFetch();
  }, []);
  const asyncFetch = () => {
    fetch('https://dev-electric.copiloto.pro/copev/public/fotonDataFrame', {
      method: 'post',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => {
        console.log('fetch data failed', error);
      });
  };
  var config = {
    data: data,
    xField: 'Event_time',
    yField: 'Value',
    seriesField: 'Category',
    yAxis: {
      label: {
        formatter: function formatter(v) {
          return ''.concat(v).replace(/\d{1,3}(?=(\d{3})+$)/g, function (s) {
            return ''.concat(s, ',');
          });
        },
      },
    },
    xAxis: { tickCount: 5 },
    slider: {
      start: 0.1,
      end: 0.5,
    },
   

  };
  return <Line {...config} />;
};


export default EnergyVsRegeneration;