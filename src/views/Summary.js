import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles'
import { Card, CardContent, Typography } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import Navbar from '../components/Navbar';
import EnergyConsumed from '../components/EnergyConsumed';
import { render } from '@testing-library/react';


const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,

    },
    iconos: {
        color: 'white'
    },
    container: {
        position: 'relative',
        marginTop: '40px',
        marginLeft: '40px',
        marginRight: '40px'

    }
}));
function Summary(props) {
    const body = {
        license_plate: ["PFTW66"],
        from: '2021-11-01' + ' 00:00:00',
        to: '2021-11-30' + ' 23:59:00'
    }
    const [data, setData] = useState([]);

    useEffect(() => {
        asyncFetch();
    }, []);
    const asyncFetch = () => {
        fetch('https://electric.copiloto.pro/copev/public/fotonSummary', {
            method: 'post',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((response) => response.json())
            .then((json) => setData(json.data[0]))
            .catch((error) => {
                console.log('fetch data failed', error);
            });
    };
    console.log(data)
    const classes = useStyles();
    render() 
        return (
            <div>
                <Grid container spacing={4}>

                    <Grid item xs={12}>
                        <Navbar title="Datos Acumulados"/>
                    </Grid>

                    <Grid item xs={12} sm={4} md={4} lg={4} xl={4} >
                        <EnergyConsumed font="white" color='#166C8C' titulo="Patente" texto={data.license_plate} />
                    </Grid>

                    <Grid item xs={12} sm={4} md={4} lg={4} xl={4} >
                        <EnergyConsumed font="white" color='#166C8C' titulo="Velocidad Promedio" texto={data.average_speed + " Km/h"} />

                    </Grid>

                    <Grid item xs={12} sm={4} md={4} lg={4} xl={4} >
                        <EnergyConsumed font="white" color='#166C8C' titulo="KM Recorridos" texto={data.km_traveled + " Km"} />

                    </Grid>

                    <Grid item xs={12} sm={4} md={4} lg={4} xl={4} >
                        <EnergyConsumed font="white" color='#166C8C' titulo="KW Cargados" texto={data.kw_load + " KW/h"} />

                    </Grid>

                    <Grid item xs={12} sm={4} md={4} lg={4} xl={4} >
                        <EnergyConsumed font="white" color='#166C8C' titulo="KW Consumidos" texto={data.consumed_energy+ " KW/h"} />

                    </Grid>

                    <Grid item xs={12} sm={4} md={4} lg={4} xl={4} >
                        <EnergyConsumed font="white" color='#166C8C' titulo="KW Regenerados" texto={data.regenerated_energy+ " KW/h"} />

                    </Grid>

                </Grid>

            </div>
        );

    }



export default Summary;