import React from 'react';
import { Grid, Paper, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Navbar from '../components/Navbar';
import 'fontsource-roboto';
import EnergyConsumed from '../components/EnergyConsumed';
import KmPerDay from '../components/KmPerDay';
import EnergyVsRegeneration from '../components/EnergyVsRegeneration';

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        height: '100%'
    },
    containerGrafica: {
        position: 'absolute',
        marginTop: '10px',
    },
    paper : {
        position: 'relative',
        marginTop: '80px',
        marginLeft: '40px',
        marginRight: '40px'
    }
}));

function Acummulatedperday(props) {
    const classes = useStyles();
    const date = new Date();
    const fecha = date.getDate() + '-'+date.getMonth()+ '-' + date.getFullYear()
    //var title = "Información día: "+fecha
    return (
        <div className={classes.root}>
            <div>
                <Navbar title="Información último período" />
            </div>
            <div>
                <Paper elevation={4} className={classes.paper}>
                    <Typography variant='h6'>&nbsp;</Typography>
                    <EnergyVsRegeneration />
                </Paper>
            </div>
        </div>
    );
}

export default Acummulatedperday;