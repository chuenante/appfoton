import React from 'react';
import { makeStyles } from '@material-ui/core/styles'
import 'fontsource-roboto';
import '../assets/css/Dashboard.css'
import FotonEventContainer from '../components/FotonEventContainer';


const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1
    }
}));

function Dashboard(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
           <FotonEventContainer />
        </div>
    );
}



Dashboard.defaultProps = {
    title: "Estado de Vehículos"
}

export default Dashboard;