import React from 'react';
import {BrowserRouter, Switch, Route, Link} from 'react-router-dom'
import Navbar from '../components/Navbar';
import Acummulatedperday from '../views/Acummulatedperday';
import Dashboard from '../views/Dashboard';
import Summary from '../views/Summary'

function App() {
  return (
    <BrowserRouter>
 
      <Switch>
       
        <Route exact path="/" ><Dashboard /></Route>
        <Route exact path="/info"  ><Acummulatedperday /></Route>
        <Route exact path="/summary" ><Summary /></Route>
      </Switch>
    </BrowserRouter>


  );
}

export default App;


